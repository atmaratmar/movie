﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
namespace movies
{
  public class Products
    {
       
        public int Id { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Genre1 { get; set; }
        public string Genre2 { get; set; }
        public string Genre3 { get; set; }
        public string Genre4 { get; set; }
        public string Genre5 { get; set; }
        public double? Rating { get; set; }
        public double? Price { get; set; }

        public  List<Products> allProducts()
        {
            string[] Products = File.ReadAllLines(@"C:\Users\DJ\Desktop\movies\data\Products.txt");
            List<Products> productsList = new List<Products>();


            for (int i = 0; i < Products.Length; i++)
            {
                string name = Products[i];
                string[] lineValues = name.Split(',');
                productsList.Add(new Products
                {
                    Id = Int32.Parse(lineValues[0]),
                    Name = lineValues[1],
                    Year = lineValues[2],
                    Genre1 = lineValues[3],
                    Genre2 = lineValues[4],
                    Genre3 = lineValues[5],
                    Genre4 = lineValues[6],
                    Genre5 = lineValues[7],
                    Rating = double.Parse(lineValues[8], CultureInfo.InvariantCulture),
                    Price = double.Parse(lineValues[9], CultureInfo.InvariantCulture),
                });



            }

            return productsList;
        }

    }
  
}
