﻿using System;
using System.Collections.Generic;
using System.IO;

namespace movies
{
   public class Users
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> Viewed { get; set; }
        public List<int> Purchased { get; set; }

        public void allUsers(out List<Users> usersList, out List<int> ViewedList)
        {
           // string[] Users = File.ReadAllLines(@"C:\Users\DJ\Desktop\movies\data\Users.txt");
            string[] Users = File.ReadAllLines(@"E:\The Academy Case Project\movies\data\Users.txt");
            usersList = new List<Users>();
            ViewedList = new List<int>();
            List<int> PurchasedList = new List<int>();

            for (int i = 0; i < Users.Length; i++)
            {
                string data = Users[i];
                string[] lineValues = data.Split(',');



                string[] lineViewedList = lineValues[2].Split(';');
                string[] linePurchasedList = lineValues[3].Split(';');

                for (int x = 0; x < lineViewedList.Length; x++)
                {
                    ViewedList.Add(Int32.Parse(lineViewedList[x]));
                }
                for (int y = 0; y < linePurchasedList.Length; y++)
                {
                    PurchasedList.Add(Int32.Parse(linePurchasedList[y]));
                }


                usersList.Add(new Users
                {
                    Id = Int32.Parse(lineValues[0]),
                    Name = lineValues[1],
                    Viewed = ViewedList,
                    Purchased = PurchasedList,
                });


            }
        }
    }

  
}
