﻿using System.Collections.Generic;
using System.IO;

namespace movies
{
   public class CurrentUserSessions
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }

       public List<CurrentUserSessions> allUserSessionsl()
        {
            string[] CurrentUserSession = File.ReadAllLines(@"C:\Users\DJ\Desktop\movies\data\CurrentUserSession.txt");
            List<CurrentUserSessions> currentUserSessions = new List<CurrentUserSessions>();


            for (int i = 0; i < CurrentUserSession.Length; i++)
            {

                string data = CurrentUserSession[i];
                string[] lineValues = data.Split(',');
                currentUserSessions.Add(new CurrentUserSessions
                {
                    UserId = int.Parse(lineValues[0]),
                    ProductId = int.Parse(lineValues[1])
                });
            }

            return currentUserSessions;
        }

    }
}
