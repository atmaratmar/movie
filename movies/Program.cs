﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace movies
{
    class Program
    {
        static void Main(string[] args)
        {
            Products products = new Products();
            Users users = new Users();
            CurrentUserSessions current_User_Sessions = new CurrentUserSessions();
            List<Products> productsList = products.allProducts();
            List<Users> usersList;
            List<int> ViewedList;
            users.allUsers(out usersList, out ViewedList);
            List<CurrentUserSessions> currentUserSessions = current_User_Sessions.allUserSessionsl();

            Console.WriteLine("//////////////// High User Review Movies/////////////////////");
            Console.WriteLine("");

            List<int> uniqueLst = ViewedList;
            uniqueLst.Sort();

            var Review = from x in uniqueLst
                    group x by x into g
                    let count = g.Count()
                    orderby count descending
                    select new { Value = g.Key, Count = count };
            int countTimes = 0;
            foreach (var movie in Review)
            {
                var HighUserReview = productsList.Where(b =>
               usersList.Any(a => movie.Value == b.Id));
               
                foreach (var item in HighUserReview)
                    {
                        Console.WriteLine("Reviewed Movies: " + movie.Count + " " + "times" + " " + " " + item.Name);
    
                    }
                if (++countTimes == 6) break;

            }



            var Current_User_Session = productsList.Where(b => currentUserSessions.Any(a => a.ProductId == b.Id));
            foreach (var User in Current_User_Session)
            {
                Console.WriteLine(">>>>>>>>>>>>>>>  Current User Sessions <<<<<<<<<<<<<<<<<<<<<<<<<");
                Console.WriteLine("");
                Console.WriteLine("Current Session::::::::::::{0} (Movie)::::::::::::::and Genre {1},{2},{3},{4},{5} ", User.Name, User.Genre1
                    , User.Genre2, User.Genre3, User.Genre4, User.Genre5);
                Console.WriteLine("");

                Console.WriteLine("____________________Recomented Movies_____________________________");


                int count = 0;

                var Recomented_movies = Current_User_Session;

                var HighUserReview = productsList.Where(b =>
                     Recomented_movies.Any(a => b.Genre1 == a.Genre1));

                foreach (var Movie in HighUserReview)
                {
                    if (User.Genre1 == Movie.Genre1|| User.Genre2 == Movie.Genre2||User.Genre3 == Movie.Genre3)
                    {
                        if (count < 5)
                        {
                            Console.WriteLine("{0}______________and Genre {1}_{2}_{3} ", Movie.Name, Movie.Genre1, Movie.Genre3, Movie.Genre4);
                            count++;
                        }
                       
                    }

                }


            }



        }

        


    }

    
}
